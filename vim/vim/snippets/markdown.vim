" bold selected text
map <leader>b S*a*jjf*i*
" italic selected text
map <leader>i  S*ajjf*
" make a markdown link from selected text
map <leader>l  S]f]a()jjh
" make a Note from a todo line
map <leader>n  llvEywwwvE@lpýbýa
