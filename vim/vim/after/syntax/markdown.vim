" Custom colors for Todo.txt like highlight
" YYYY-MM-DD
syntax  match  TodoDate       '\d\{2,4\}-\d\{2\}-\d\{2\}'
" YYYY/MM/DD
syntax  match  TodoDate       '\d\{2,4\}/\d\{2\}/\d\{2\}'
" MM-DD
syntax  match  TodoDate       '\d\{2\}-\d\{2\}'
" MM/DD
syntax  match  TodoDate       '\d\{2\}/\d\{2\}'
" Data YYYY/Day-of-year
syntax  match  TodoDate       '\d\{4\}/\d\{3\}'
" Data YY/Day-of-year
syntax  match  TodoDate       '\d\{2\}/\d\{3\}'

syntax  match  TodoDueDate    'due:\d\{2,4\}-\d\{2\}-\d\{2\}'
syntax  match  TodoDueDate    'due:\d\{2\}-\d\{2\}'
syntax  match  TodoDueDate    'due:\d\{2\}/\d\{1,3\}'
syntax  match  TodoProject    '\(^\|\W\)+[^[:blank:]]\+'
syntax  match  TodoContext    '\(^\|\W\)@[^[:blank:]]\+'
syntax  match  TodoTag        '\(^\|\W\)#[^[:blank:]#]\+'

hi def link TodoDate       String
hi def link TodoProject    Statement
hi def link TodoContext    Constant
hi def link TodoTag        Error
