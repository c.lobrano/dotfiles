# Save this file in $PsHome to have git, vim and VS native command prompt in powershell
Set-Alias gvim ' C:\Program Files (x86)\Vim\vim81\gvim.exe'
Set-Alias git ' C:\Program Files\Git\bin\git.exe'

pushd 'C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\VC\Auxiliary\Build'
cmd /c "vcvars64.bat&set" |
foreach {
  if ($_ -match "=") {
    $v = $_.split("="); set-item -force -path "ENV:\$($v[0])"  -value "$($v[1])"
  }
}
popd
write-host "`nVisual Studio 2010 Command Prompt variables set." -ForegroundColor Yellow
