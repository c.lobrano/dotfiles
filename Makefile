## This makefile belongs to the suite of file for configure, install, change and remove my dotfiles

SUBDIRS = bash neovim vim fonts

.PHONY: deps subdirs $(SUBDIRS)

#--------------------------------------------------------
# Install programs and dependencies
deps:
	./batch-install.sh requirements.txt


#--------------------------------------------------------
# Configure/Install subdirs
subdirs: $(SUBDIRS) ## Install all subdirectories

$(SUBDIRS):
	$(MAKE) -C $@


#--------------------------------------------------------
# Clean subdirs
SUBCLEAN = $(addsuffix .clean, $(SUBDIRS))

.PHONY: clean $(SUBCLEAN)

clean: $(SUBCLEAN) ## Clean all (subdirectories included)

$(SUBCLEAN): %.clean:
	$(MAKE) -C $* clean




