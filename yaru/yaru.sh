#/usr/bin/bash


sudo apt install -y libgtk-3-dev gtk-3-examples meson sassc

gsettings set org.gtk.Settings.Debug enable-inspector-keybinding true

mkdir -p ~/workspace
git clone https://github.com/ubuntu/yaru.git ~/workspace/yaru

git clone https://github.com/clobrano/script-fu.git ~/workspace/script-fu
sudo apt install -y expect

update-alternatives --install /usr/share/gnome-shell/theme/gdm3.css gdm3.css /usr/share/gnome-shell/theme/Yaru/gnome-shell.css 15
